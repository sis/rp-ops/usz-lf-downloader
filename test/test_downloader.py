from unittest import TestCase

import importlib

module = importlib.import_module("usz-lf-downloader")


class TestConfig(TestCase):
    def test_config(self):
        cfg = {
            "api_key": "AANCqlc5xDBC5GSIxkny17",
            "host": "https://transfer.usz.ch",
            "dest_folder": "/tmp/",
            "folder": "/messages/inbox",
            "extra_log_dir": "/tmp/",
            "delete_attachments": False,
        }
        cfg_obj = module.Config.from_dict(cfg)
        self.assertEqual(
            cfg_obj,
            module.Config(
                api_key="QUFOQ3FsYzV4REJDNUdTSXhrbnkxNzp4",
                host="https://transfer.usz.ch",
                dest_folder="/tmp/",
                folder="/messages/inbox",
                extra_log_dir="/tmp/",
                delete_attachments=False,
                file_size_limit=1024 ** 4,
            ),
        )

    def test_load_config(self):
        cfg = module.load_config("config.sample.py")
        self.assertEqual(cfg.dest_folder, "/tmp/")


class TestDownloader(TestCase):
    def test_sanitize(self):
        self.assertEqual(module.sanitize("../abc"), "._abc")
        self.assertEqual(module.sanitize("../\abc"), ".__bc")
