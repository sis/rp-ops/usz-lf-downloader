# Overview

Download data from the LiquidFiles server at USZ

This Python package contains a single script
``usz-lf-downloader.py`` downloading data packages from the
LiquidFiles server at USZ [transfer.usz.ch](https://transfer.usz.ch).
As the LiquidFiles API does not allow deleting files (attachments),
the sender at USZ assures that attachments only can be downloaded
once. Further attempts to download the file will result in a 422
response.
The script will simply skip those attachments.

# Usage

You can start the script by calling the entry point script
``usz-lf-downloader.py <config_file>``.

Example

```bash
usz-lf-downloader.py config.sample.py
```


# Configuration file

The download script needs to be fed a python
configuration file

```python
api_key = "AANCqlc5xDBC5GSIxkny17"
host = "https://transfer.usz.ch"
dest_folder = "/tmp/"
folder = "/messages/inbox"
extra_log_dir = "/tmp/"
delete_attachments = False
```

The meaning of the configuration items is as follows:

- ``api_key``
	The API key, obtained from your LiquidFiles account -> Settings -> API
- ``host``
	The https endpoint of the LiquidFiles server
- ``dest_folder``
	The folder to store downloaded files into
- ``folder``
	The message folder on the LiquidFiles server
- ``extra_log_dir``
	A folder where html bodies of errors from the LiquidFiles server
	will be stored
- ``delete_attachments``
	If set to `true` try to delete attachments remotely on the
	LiquidFiles server after successful download
- ``file_size_limit``
    Cancel download of an attachment after this size limit


# Installation

```bash
pip install git+https://gitlab.ethz.ch/sis/rp-ops/usz-lf-downloader.git
```


# Development

To run the all tests run

```bash
./setup.py test
```
