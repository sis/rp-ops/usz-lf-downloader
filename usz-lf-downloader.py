#!/usr/bin/env python3
"""Fetch files from transfer.usz.ch using the liquidfiles software

Dependencies:
    * python 3
    * When using a SOCKS proxy, Anorov's PySocks module must be installed
        (https://github.com/Anorov/PySocks; pip install PySocks)

Usage:
    Without proxy:
    {script} [-v] <config_file>

    With proxy: set the ALL_PROXY env var to point to your SOCKS proxy server and port
"""


import os
import sys
import base64
import json
import re
import hashlib
import logging
import urllib.request
import urllib.error
import urllib.parse
import traceback
import importlib.util
from datetime import datetime
from typing import Optional, Callable, Tuple

from dataclasses import dataclass, field, fields

__version__ = "1.0.0"


def check_https(s: str) -> str:
    if not s.startswith("https://"):
        raise ValueError("Invalid host: " + s)
    return s


def check_api_key(s: str) -> str:
    if not re.compile(r"^[a-zA-Z0-9]*$").fullmatch(s):
        raise ValueError("Invalid API key: " + s)
    try:
        return base64.b64encode(bytes(s, "utf-8") + b":x").decode()
    except UnicodeError as e:
        raise ValueError("Invalid API key: " + s) from e


def check_writable_dir(s: str) -> str:
    if not os.path.isdir(s):
        raise FileNotFoundError(f"Folder `{s}` not found.")
    if not os.access(s, os.W_OK):
        raise PermissionError(f"cannot write into destination folder `{s}`")
    return s


def optional(f: Callable) -> Callable:
    """Turns a strict check function into a check function only
    checking on values distinct from `None`"""

    def wrapped(s: Optional[str]) -> Optional[str]:
        if s is None:
            return s
        return f(s)

    return wrapped


@dataclass
class Config:
    dest_folder: str = field(metadata=dict(convert=check_writable_dir))
    host: str = field(metadata=dict(convert=check_https))
    api_key: str = field(metadata=dict(convert=check_api_key))
    folder: str = "/messages/inbox"
    extra_log_dir: Optional[str] = field(
        default=None, metadata=dict(convert=optional(check_writable_dir))
    )
    delete_attachments: bool = False
    file_size_limit: int = 1024 ** 4  # 1TB

    @classmethod
    def from_dict(cls, d: dict):
        converters = {f.name: f.metadata.get("convert", f.type) for f in fields(cls)}
        validated_data = {
            key: converters.get(key, lambda x: x)(val) for key, val in d.items()
        }

        return cls(**validated_data)


def load_config(config_file: str) -> Config:
    spec = importlib.util.spec_from_file_location("config", config_file)
    if spec is None:
        raise RuntimeError("Could not load conig")
    mod = importlib.util.module_from_spec(spec)
    exec_module = getattr(spec.loader, "exec_module", None)
    if exec_module is None:
        raise RuntimeError("Cannot load config")
    exec_module(mod)
    return Config.from_dict(
        {key: val for key, val in vars(mod).items() if not key.startswith("_")}
    )


TIMESTAMP_FMT = "%Y-%m-%dT%H:%M:%S.%f%z"


def run(cfg: Config):
    """Core functionality - no CLI
    Downloads all messages from host according to `cfg`

    :param cfg: Service config
    """
    proxy_env = os.environ.get("ALL_PROXY")
    if proxy_env is not None:
        url_parts = urllib.parse.urlparse(proxy_env)
        proxy_settings: Optional[Tuple[str, Optional[str], Optional[int]]] = (
            url_parts.scheme,
            url_parts.hostname,
            url_parts.port,
        )
    else:
        proxy_settings = None
    # Opener based on proxy settings: Normal opener if no proxy
    # settings are given,
    # PySocks opener if proxy settings are given:
    opener = build_opener(proxy_settings)

    logging.debug("Querying host: %s", cfg.host)
    try:
        messages = get_message_list(
            opener, host=cfg.host, folder=cfg.folder, api_key_base64=cfg.api_key
        )
    except Exception as e:  # pylint: disable=broad-except
        logging.warning("Failed to fetch: %s", e)
        return
    logging.debug(
        "Got message list:\n%s", json.dumps(messages, indent=2, sort_keys=True)
    )

    attachments = [
        (a, msg) for msg in messages["messages"] for a in msg.get("attachments", [])
    ]
    logging.debug("Number of attachments: %d", len(attachments))
    for att, msg in attachments:
        try:
            download(
                att,
                msg,
                opener=opener,
                api_key_base64=cfg.api_key,
                download_root=cfg.dest_folder,
                file_size_limit=cfg.file_size_limit,
            )
        except Exception as err:  # pylint: disable=broad-except
            handle_error(
                err,
                level=logging.ERROR,
                log_file=cfg.extra_log_dir
                and os.path.join(cfg.extra_log_dir, f"html-err-{msg['id']}.html"),
            )
    # Looks like we don't have the permission to delete on LF
    if cfg.delete_attachments:
        for msg in messages:
            delete_attachments(cfg.host, msg, cfg.api_key)


def get_message_list(
    opener,
    *,
    api_key_base64: str,
    host: str = "https://transfer.usz.ch",
    folder: str = "/messages/inbox",
):
    with liquid_stream(
        url_join(host, folder), api_key_base64, opener, accept="application/json"
    ) as list_stream:
        return json.loads(list_stream.read().decode())


def download(
    attachment,
    message,
    *,
    opener,
    api_key_base64: str,
    download_root: str = "./",
    file_size_limit=1024 ** 4,
):
    """Downloads all attachments into download_root"""

    attachment_url = attachment["url"]
    attachment_name = sanitize(attachment["filename"])
    logging.debug(
        "id: %s, sender: %s, url: %s", message["id"], message["sender"], attachment_url
    )
    envelope_dir = sanitize(f"{message['id']}-{attachment['id']}")
    envelope_path = os.path.join(download_root, envelope_dir)
    att_path = os.path.join(envelope_path, attachment_name)
    logging.info(
        "Downloading attachment %s (message id: %s" ", attachment id: %s, url: %s)",
        attachment_name,
        message["id"],
        attachment["id"],
        attachment_url,
    )
    try:
        os.mkdir(envelope_path)
    except FileExistsError:
        logging.info(f"Destination folder {envelope_path} already exists. Skipping")
        return
    try:
        with open(att_path, "wb") as f, liquid_stream(
            attachment_url, api_key_base64, opener, accept="application/json, */*;q=0.8"
        ) as stream:
            copyfileobj(stream, f, limit=file_size_limit)
        compare_checksum(att_path, attachment["checksum"])
    except Exception:  # pylint: disable=broad-except
        try:  # General error handling first
            try:
                os.unlink(att_path)
                os.rmdir(envelope_path)
            except OSError as err:
                logging.warning(f"Could not delete folder {envelope_path}: {err}")
            raise
        except urllib.error.HTTPError as err:  # Specialized error handling here
            # Check for attachments already downloaded and thus blocked
            # (the API does not allow to delete attachments):
            if err.code == 422:  # Unprocessable Entity
                logging.info("Download limit exceeded - skipping")
                return
            raise
    with open(os.path.join(envelope_path, "done.txt"), "wb") as f_done:
        f_done.write(b"")


def delete_attachments(host, msg, api_key_base_64, opener=urllib.request.urlopen):
    del_url = url_join(host, "message", msg["id"], "delete_attachments")

    request = urllib.request.Request(
        del_url,
        headers={
            "Authorization": "Basic " + api_key_base_64,
            "Content-Type": "application/json",
            "Accept": "application/json, */*;q=0.8",
        },
    )
    logging.debug(del_url)
    request.get_method = lambda: "DELETE"
    try:
        response = opener(request)
        logging.info("Deleted attachments: %s", del_url)
        return response

    except urllib.error.HTTPError as err:
        logging.warning(
            "Could not delete remote attachment: %s", handle_http_error(err)
        )
    return None


def url_join(*parts):
    return "/".join(s.strip("/") for s in parts)


def build_opener(proxy_settings):
    """
    Returns an opener based on proxy settings: Normal opener if no proxy
    settings are given, PySocks opener if proxy settings are given
    """
    if proxy_settings is None:
        return urllib.request.urlopen
    try:
        import socks  # type: ignore # pylint: disable=import-outside-toplevel
        from sockshandler import (  # type: ignore # pylint: disable=import-outside-toplevel
            SocksiPyHandler,
        )
    except ModuleNotFoundError as e:
        raise ValueError(
            "Module PySocks needs to be available when $ALL_PROXY is set"
        ) from e
    protocol, host, port = proxy_settings
    protocol_mapping = {"socks4": socks.SOCKS4, "socks5": socks.SOCKS5}
    try:
        opener = urllib.request.build_opener(
            SocksiPyHandler(protocol_mapping[protocol], host, port)
        ).open

        def try_open(*args, **kwargs):
            try:
                return opener(*args, **kwargs)
            except urllib.error.URLError as crash:
                if isinstance(crash.reason, socks.ProxyConnectionError):
                    raise RuntimeError(
                        "Connection to proxy server refused. " "Is the proxy running?"
                    ) from crash
                raise

        return try_open
    except KeyError:
        raise ValueError("Don't know the protocol " + protocol) from None


def liquid_stream(url, api_key_base_64, opener, *, accept):
    """Calls the url for the liquidfiles API,
    "Accept" headers have to be added after the call (different)"""
    request = urllib.request.Request(
        url,
        headers={
            "Authorization": "Basic " + api_key_base_64,
            "Content-Type": "application/json",
        },
    )
    request.add_header("Accept", accept)
    return opener(request)


def compare_checksum(filename, checksum):
    download_checksum = compute_sha256(filename)
    if download_checksum == checksum:
        logging.info("File %s has expected checksum %s", filename, download_checksum)
        return True
    logging.warning(
        "Checksum mismatch for file `%s`: got %s, expected: %s",
        filename,
        download_checksum,
        checksum,
    )
    return False


def compute_sha256(filename, block_size=65536):
    hash_sha256 = hashlib.sha256()
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(block_size), b""):
            hash_sha256.update(block)
    return hash_sha256.hexdigest()


def sanitize(path: str) -> str:
    """Make sure path component does not contain escape sequences and ../"""
    path = re.sub(r"[^a-zA-Z0-9_.-]", "_", path)
    return re.sub(r"\.\.+", ".", path)


def cli():
    """CLI entry point"""

    import argparse  # pylint: disable=bad-option-value,import-outside-toplevel

    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s")
    logger = logging.getLogger()
    logger.propagate = True
    parser = argparse.ArgumentParser(prog="LiquidFiles downloader")
    parser.add_argument("--version", action="version", version=__version__)

    parser.add_argument("config", type=str, help="Location of the config file")
    parser.add_argument(
        "-v",
        "--verbose",
        dest="log_level",
        help="increase verbosity",
        action="store_const",
        const=logging.DEBUG,
        default=logging.INFO,
    )

    args = parser.parse_args()
    logger.setLevel(args.log_level)
    cfg = load_config(args.config)
    timestamp = datetime.now().astimezone()
    logging.debug("Start running at %s", timestamp.strftime(TIMESTAMP_FMT))

    try:
        run(cfg)
    except Exception as err:  # pylint: disable=broad-except
        handle_http_error(
            err,
            log_file=os.path.join(
                cfg.extra_log_dir, f"html-err-{timestamp.strftime(TIMESTAMP_FMT)}.html"
            ),
        )
        sys.exit(os.EX_SOFTWARE)

    sys.exit(os.EX_OK)


def handle_error(err, log_file=None, level=logging.FATAL):
    try:  # For further error distinction including `finally`:
        if isinstance(err, urllib.error.HTTPError):
            logging.log(
                level,
                "Fatal error during processing: %s",
                handle_http_error(err, log_file=log_file),
            )
        else:
            logging.log(level, "Fatal error during processing: `%s`", err)
    finally:
        traceback.print_exc()


def handle_http_error(err, log_file=None) -> str:
    """Tries to parse the error response body as json.
    On failure, assume that response is html formatted and save to log_file
    """
    msg = err.fp.read()
    try:
        err_json = json.loads(msg.decode())
        return f"`{err}`\n\t" + "\n\t".join(e["error"] for e in err_json)
    except json.JSONDecodeError:
        if log_file:
            with open(log_file, "w", encoding="utf-8") as f:
                f.write(msg)
            return (
                f"`{err}`\n\t"
                "Could not parse response as json. Storing response as " + log_file
            )
        return format(err)
    except UnicodeError:
        return f"`{err}`\n\t" "Could not decode error body: " + str(msg)


def copyfileobj(fsrc, fdst, length=16 * 1024, limit=1024 ** 4):
    """Replacement for shutil.copyfileobj including a size limit"""
    bytes_copied = 0
    while bytes_copied < limit:
        buf = fsrc.read(length)
        if not buf:
            break
        fdst.write(buf)
        bytes_copied += length
    if bytes_copied >= limit:
        raise IOError(f"Reached size limit of {limit} b")


if __name__ == "__main__":
    cli()
